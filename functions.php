<?php
function getAsciiChar()
{
	$asciiChar=random_int(48, 122);
	if( $asciiChar > 57 && $asciiChar < 65 )
		return getAsciiChar();
	elseif ( $asciiChar > 90 && $asciiChar < 97 )
		return getAsciiChar();
	else
		return $asciiChar;
}
function getRandName()
{
	$numCaracteres=random_int(32, 84);
	$str="";
	for ( $i=0; $i<$numCaracteres; $i++ )
	{
		$str.=chr(getAsciiChar());
	}
	$str.=".png";
	return $str;
}

function getRandNameToch()
{
	$numCaracteres=random_int(32, 84);
	$str="";
	for ( $i=0; $i<$numCaracteres; $i++ )
	{
		$str.=chr(getAsciiChar());
	}
	$str.="";
	return $str;
}

function fullPicPath( $folder )
{
	if( strcmp( substr($folder, -1),  "/" ) == 0 )
		return $folder.getRandName();
	else
		return $folder."/".getRandName();
}

function fullPicPathToch( $folder )
{
	if( strcmp( substr($folder, -1),  "/" ) == 0 )
		return $folder.getRandNameToch();
	else
		return $folder."/".getRandNameToch();
}

?>
