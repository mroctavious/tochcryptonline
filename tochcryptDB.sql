DROP TABLE IF EXISTS tc_encrypts;
DROP TABLE IF EXISTS tc_logins;
DROP TABLE IF EXISTS tc_users;
DROP PROCEDURE IF EXISTS addUser;
DROP PROCEDURE IF EXISTS unlockUser;
DROP PROCEDURE IF EXISTS loginTryAdd;
DROP TRIGGER IF EXISTS updateStatus;

##TABLA DE USUARIOS
CREATE TABLE tc_users
(
	id	INT PRIMARY KEY  AUTO_INCREMENT,
    user VARCHAR(50) NOT NULL UNIQUE,
    passwd VARCHAR(1000) NOT NULL,
    email   VARCHAR(100) NOT NULL,
    status TINYINT(1) NOT NULL,
    fTries TINYINT(7) NOT NULL
);


##Tabla que tendra registro de log in
CREATE TABLE tc_logins
(
		logId INT PRIMARY KEY AUTO_INCREMENT,
        idUser  INT NOT NULL,
        tryDate TIMESTAMP NOT NULL,
        successLog  TINYINT(1) NOT NULL,
        
        FOREIGN KEY (idUser)
			REFERENCES tc_users(id)
            ON DELETE CASCADE
);

DROP TABLE tc_encrypts;

##Tabla principal que contendra la ubicacion de las imagenes
CREATE TABLE tc_encrypts
(
	idEncrypt INT PRIMARY KEY AUTO_INCREMENT,
        idUser  INT NOT NULL,
        idLog  INT NOT NULL,
        title   VARCHAR(120),
        picture VARCHAR(300),
		FOREIGN KEY (idUser)
			REFERENCES tc_users(id)
            ON DELETE CASCADE,

        FOREIGN KEY (idLog)
			REFERENCES tc_logins(logId)
            ON DELETE CASCADE

);


##Procedimiento que agrega nuevo usuario a la tabla
DELIMITER $$
CREATE PROCEDURE addUser( IN username VARCHAR(50), IN pass VARCHAR(300),  IN mail VARCHAR(100) )
BEGIN
	INSERT INTO tc_users( user, passwd, email, status, fTries ) VALUES ( username, PASSWORD(pass), mail, 1, 0 );
END $$

##Crea registro de intento de log in
DELIMITER $$
CREATE PROCEDURE loginTryAdd( _idUser INT, success TINYINT )
BEGIN
	INSERT INTO tc_logins(idUser, successLog)  VALUES ( _idUser, success );
END $$

##Procedimiento que Desbloquea un usuario
DELIMITER $$
CREATE PROCEDURE unlockUser( username VARCHAR(50) )
BEGIN
	UPDATE tc_users SET fTries=0 WHERE user = username;
END $$


##Funcion que verifica el inicio de sesion
## 1 => Inicio Correctamente
## -1 => Contraseña incorrecta
## 0 => No se encontro usuario
## -2 => Cuenta bloqueada
DROP FUNCTION IF EXISTS login;
DELIMITER $$
CREATE FUNCTION login( username VARCHAR(50), pass VARCHAR(400) )
RETURNS INT
BEGIN
	##Conseguir datos del usuario
	DECLARE idUser INT;
    DECLARE userStatus TINYINT(1);
	DECLARE  vPass VARCHAR(400);
    
    ##Verificar usuario si existe
	SELECT id, passwd, status INTO idUser, vPass, userStatus FROM 
	tc_users WHERE 	user = username;
	
    ##En caso de que no se encuentre usuario
    IF idUser IS NULL THEN
		RETURN -1;
        
	##En caso de que el usuario este bloqueado
	ELSEIF userStatus = 0 THEN
		RETURN -2;
        
	##En caso de que la contraseña este correcta
	ELSEIF PASSWORD(pass) = vPass THEN
		call loginTryAdd( idUser, 1 );
        UPDATE tc_users SET fTries=0 WHERE id = idUser;
		RETURN idUser;
        
	##En caso de equivocarse
	ELSE
		call loginTryAdd( idUser, 0 );
        ##Se aumenta el contador de intentos fallidos
        UPDATE tc_users SET fTries=fTries+1 WHERE id = idUser;
		RETURN 0;
        
	END IF;
    
END $$


###TRIGGERS
##Trigger que analizara el numero de intentos fallidos, en caso de que haya mas de 5,
##Entonces se bloquea la cuenta
DELIMITER $$
CREATE TRIGGER updateStatus BEFORE UPDATE ON tc_users
FOR EACH ROW BEGIN
		IF NEW.fTries > 5 THEN
			SET NEW.status=0;
		ELSEIF NEW.fTries = 0 THEN 
			SET NEW.status=1;
        END IF;
END$$


DROP PROCEDURE IF EXISTS addPic;


##procedimiento que relacionara la imagen encryptada con un log in
DELIMITER $$
CREATE PROCEDURE addPic( idUser INT, pTitle VARCHAR(100), pictureLoc VARCHAR(150) )
BEGIN
	DECLARE idLog INT;	
	SELECT logId INTO idLog FROM tc_logins WHERE idUser=idUser ORDER BY tryDate DESC LIMIT 1;
    INSERT INTO tc_encrypts(idUser, idLog, title, picture) VALUES( idUser, idLog, pTitle, pictureLoc);

END$$

##�Procedimiento para borrar imagen de la tabla
DELIMITER $$
CREATE PROCEDURE rmPic( idEnc INT )
BEGIN
	DELETE FROM tc_encrypts WHERE idEncrypt=idEnc;
END$$


###############################################################################
## TESTING ALL ###

##Add user
call addUser( 'mroctavious', '1127Octo', 'erodriguez35@alumnos.uaq.mx');

##Intentar iniciar sesion
SELECT login( 'mroctavious', '1127Octo');

SELECT * FROM tc_users;
CALL addUser('mroctavious', '1127Octo', 'erod@hotmail.com');
##Desbloquear usuario
CALL unlockUser('mroctavious');
SELECT * FROM tc_logins;

SELECT login('$username', '$pass' ) AS logRes;


DELETE FROM tc_users WHERE id=7;

DELETE  FROM tc_encrypts WHERE idUser=3;

call addPic(1, 'pko1', 'U4vWGZSmbFD7Sx7aeU6eGNthud0mH2KZoJoGth7Y8nQaGNBequzFPKUpdsXooVbUZsnpyxT0GRHVVCqOvUVB.png');
call rmPic(8);

DELETE FROM tc_encrypts WHERE idEncrypt = 11;
SELECT * FROM tc_encrypts;
SELECT * FROM tc_encrypts WHERE idUser=8;
