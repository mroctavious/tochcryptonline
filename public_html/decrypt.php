<?php
        session_start();

        #Validamos si el usuario quiere cerrar sesion
        if(isset($_GET['salir']))
        {
                #Destruimos todas las variables de sesion
                session_unset();
        }

        if(isset($_SESSION['id']))
        {
                require_once('../conexion.php');
		$_SESSION['encPic']="";


?>

<!DOCTYPE html>
<head>
<title>Tochcrypt!</title>
	<link rel="stylesheet" href="style.css" />
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed|Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="scriptD.js"></script>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">


<style>
#grad1 {
    height: 200px;
    background: green; /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left, green , gray); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(right, green, gray); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(right, green, gray); /* For Firefox 3.6 to 15 */
    background: linear-gradient(to right, green , gray); /* Standard syntax (must be last) */
}
</style>
<link href="css/main.css" rel="stylesheet" type="text/css" />

<center><img src="Tochpan.png" alt="Tochcrypt" height="200" width="150"><font size="50" color="red"><h1>TochCrypt!</h1></font></center>

</head>
<?php
}
else
{
	header("Location: index.php");
}
?>

<style>
.mySlides {display:none}
.demo {cursor:pointer}
</style>
<body>
    <div class="container">
		<ul></ul>
            <ul id="nav">
                <li><a href="miTochpan.php">Mi TochCrypt</a></li>

                <li><a href="encrypt.php">ENCRYPT</a>


                </li>


                <li> <a href="decrypt.php">DECRYPT</a></li>
		<span id="s1"></span>

            </ul>

        </div>
<br><br><br><br>


<br><br>
<center>
<table>
	<TR>
		<TD align="center"><hpko2>DECRYPTING MODE</hpko2></TD>

	</TR>
	<TR class="spaceUnder">
		<TD>	<div id="image_preview"><img id="previewing" src="noimage.png"></div></TD>
			<td align="left"><div id="message"></div></td>

	</TR>
	<TR></TR>
	<TR>
		<td align="left">			<h4 id='loading' >Encrypting....<div id="loaderImage"></div></h4>  </td>

	</TR>

</table>
</center>
	<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
		 <div id="loaderImage"></div>

	<center>
			<div id="selectImage">

				<label>Selecciona Imagen que desea encriptar [JPG, PNG]</label><br/>
				<input type="file" name="file" id="file" required />
				<input type="file" name="tochFile" id="tochFile" required />
				<input type="submit" value="Upload" class="submit" />

			</div>


	</center>
	</form>



</body>
<script type="text/javascript">
	var cSpeed=2;
	var cWidth=130;
	var cHeight=130;
	var cTotalFrames=20;
	var cFrameWidth=130;
	var cImageSrc='images/sprites.gif';

	var cImageTimeout=false;
	var cIndex=0;
	var cXpos=0;
	var cPreloaderTimeout=false;
	var SECONDS_BETWEEN_FRAMES=0;

	function startAnimation(){
		document.getElementById('loaderImage').style.backgroundImage='url('+cImageSrc+')';
		document.getElementById('loaderImage').style.width=cWidth+'px';
		document.getElementById('loaderImage').style.height=cHeight+'px';
		//FPS = Math.round(100/(maxSpeed+2-speed));
		FPS = Math.round(100/cSpeed);
		SECONDS_BETWEEN_FRAMES = 1 / FPS;
		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES/1000);
	}
	function continueAnimation(){
		cXpos += cFrameWidth;
		//increase the index so we know which frame of our animation we are currently on
		cIndex += 1;
		//if our cIndex is higher than our total number of frames, we're at the end and should restart
		if (cIndex >= cTotalFrames) {
			cXpos =0;
			cIndex=0;
		}
		if(document.getElementById('loaderImage'))
			document.getElementById('loaderImage').style.backgroundPosition=(-cXpos)+'px 0';
		cPreloaderTimeout=setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES*1000);
	}

	function stopAnimation(){//stops animation
		clearTimeout(cPreloaderTimeout);
		cPreloaderTimeout=false;
	}

	function imageLoader(s, fun)//Pre-loads the sprites image
	{
		clearTimeout(cImageTimeout);
		cImageTimeout=0;
		genImage = new Image();
		genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
		genImage.onerror=new Function('alert(\'Could not load the image\')');
		genImage.src=s;
	}

	//The following code starts the animation
	new imageLoader(cImageSrc, 'startAnimation()');
</script>

</html>












