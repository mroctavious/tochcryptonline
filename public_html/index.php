<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Tochcrypt</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<?php

session_start();
	# Mandamos llamar el archivo de conexion
	require_once('../conexion.php');

	# Validamos si se dio click en el boton registrar
	if(isset($_POST ['btnRegistrar']))
	{

		$username=$_POST['nuevoUsername'];
		$pass1=	$_POST['pass1'];
		$pass2=	$_POST['pass2'];
		$email=	$_POST['email'];

		#Validamos que las contraseñas sean iguales
		if ($pass1==$pass2)
		{
			#Registramos al usuario Creando un INSERT
			$insert="call addUser( '$username', '$pass2', '$email');";
			#echo $insert;
			#Lo enviamos a la base de datos
			$conexion->query($insert);
			echo "<script type='text/javascript'>alert('Usuario registrado correctamente')</script>";
		}
		else
		{
			echo "<script type='text/javascript'>alert('Contraseñas no coinciden!')</script>";
		}
	}

	#validamos si se dio click en el boton inciar sesion
	if(isset($_POST['btnIniciarSesion']))
	{
		#recuperamos las variables del formulario de incio de sesion
		$username=	$_POST['username'];
		$pass=		$_POST['pass'];

		#Creamos consulta que busque un usuario con los datos del formulario
		$buscarUsuario=	"SELECT login('$username', '$pass' ) AS logRes";
		#echo $buscarUsuario;
		$resultado= mysqli_query($conexion, $buscarUsuario);
		#Contamos el numero de filas que arrojo la consula, si no arroja ninguna significa que contraseña o usuario son incorrectos
		$filas=$resultado->num_rows;
		#echo $filas;
		#recuperamos la consulta
		$usuario= $resultado->fetch_assoc();
		if($filas>0)
		{
			if( $usuario['logRes'] > 0 )
			{
				#echo"Puedes iniciar sesion <br />";
				#Se crea variable de sesion
				$_SESSION['id']= $usuario['logRes'];
				header("Location: miTochpan.php");
			}
			elseif( $usuario['logRes'] == 0 )
			{
				echo "<script type='text/javascript'>alert('Usuario no existe!')</script>";

			}
			elseif( $usuario['logRes'] == -1 )
			{
				echo "<script type='text/javascript'>alert('Usuario o Contrase  a invalidos')</script>";
			}
			elseif( $usuario['logRes'] == -2 )
			{
				echo "<script type='text/javascript'>alert('Usuario BLOQUEADO !')</script>";

			}
		}
		else
		{
			echo "<script type='text/javascript'>alert('Error desconocido!')</script>";
		}
	}


?>


<body>
  <center><img src="Tochpan.png" alt="Tochcrypt" height="300" width="250"><br><h1><font size="25" color="red">TochCrypt!</font></h1></center>

  <div class="form">
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Registrate!</h1>
          
          <form action="/" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Username<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" name="nuevoUsername" maxlength="50">
            </div>
        </div><br><br>
          <div class="field-wrap">
            <label>
              Email<span class="req">*</span>
            </label>
            <input type="email" required autocomplete="off" name="email">
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off" name="pass1">
          </div>
          

	<div class="field-wrap">
            <label>
              Repeat Password<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off" name="pass2">
          </div>
          <button type="submit" class="button button-block" name="btnRegistrar">Start!</button>
          
          </form>

        </div>
        
        <div id="login">   
          <h1>Bienvenido</h1>
          
          <form action="/" method="post">
          
            <div class="field-wrap">
            <label>
              Username<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="off" name="username">
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off" name="pass">
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button class="button button-block" name="btnIniciarSesion">Log In</button>
          
          </form>

        </div>
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
  <script type="text/javascript"  src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script type="text/javascript"  src="js/index.js"></script>

</body>
</html>
