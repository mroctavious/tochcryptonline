<?php
        session_start();

        #Validamos si el usuario quiere cerrar sesion
        if(isset($_GET['salir']))
        {
                #Destruimos todas las variables de sesion
                session_unset();
        }

        if(isset($_SESSION['id']))
        {
                require_once('../conexion.php');
		$id_user=$_SESSION['id'];
		$select="SELECT * FROM tc_encrypts WHERE idUser=$id_user;";
		$result=$conexion->query($select);
		$rows = array();
		#$rows[] = $row;}
		#$contador=1;

?>

<!DOCTYPE html>
<head>
    <title>Mi Tochcrypt</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<style>
#grad1 {
    height: 200px;
    background: green; /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left, green , gray); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(right, green, gray); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(right, green, gray); /* For Firefox 3.6 to 15 */
    background: linear-gradient(to right, green , gray); /* Standard syntax (must be last) */
}
</style>
<link href="css/main.css" rel="stylesheet" type="text/css" />
  
<center><img src="Tochpan.png" alt="Tochcrypt" height="200" width="150"><font size="25" color="red"><b><h1>TochCrypt!</b></h1></font></center>

</head>

<style>
.mySlides {display:none}
.demo {cursor:pointer}
</style>
<body>
<center>
    <div class="container">

		<ul></ul>
            <ul id="nav">
                <li><a href="miTochpan.php">Mi TochCrypt</a></li>

                <li><a href="encrypt.php">ENCRYPT</a>

                    <span id="s1"></span>


                </li>


                <li><a href="decrypt.php">DECRYPT</a></li>


            </ul>

        </div>
</center>
<br><br><br><br>

<div class="w3-container">
<center> <b> <h1>Mis Imagenes Encriptadas</h1> </b></center>
</div>
<br><br>
<div class="w3-content" style="max-width:800px">
		    <div class="derecha">
			    <ul id="galeria">
			<?php
				while($row= $result->fetch_assoc())
				{
					$pic=$row['picture'];
					echo "<li><img src='$pic'  title='' /></li>";
				}
			?>
			      </ul>
		    </div>

<?php
	foreach($rows as $row)
	{
		echo $row['llave'];
 		echo "<img class='mySlides' src=".$row['location']." style='width:100%'>";
	}
	echo "<div class='w3-row-padding w3-section'>";
	foreach($rows as $row)
	{

		echo "<div class='w3-col s4'>";
      		echo "<img class='demo w3-opacity w3-hover-opacity-off' src=".$row['location']." style='width:100%' onclick='currentDiv(".$contador.")'>";
		echo "<br><b>".$row['name']."</b>";

		echo "</div>";
		$contador+=1;

	}
echo "</div></div>";

?>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " w3-opacity-off";
}
</script>

</body>
</html>
<?php
}
else
{
	header("Location: index.php");
}
?>
